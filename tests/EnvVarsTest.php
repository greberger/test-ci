<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EnvVarsTest extends KernelTestCase
{
    public function testEnvVarsAreIdentical(): void
    {

        $kernel = self::bootKernel();

        $this->assertSame('test', $kernel->getEnvironment());
        $this->assertSame('https://www.zohoapis.eu/inventory/v1', $_ENV['ZOHO_INVENTORY_API']);
    }
}
