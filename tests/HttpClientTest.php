<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HttpClientTest extends KernelTestCase
{
    public function testSomething(): void
    {
        $kernel = self::bootKernel();

        $client = $kernel->getContainer()->get('http_client');
        $response = $client->request('GET', 'https://3kd.be');

        $this->assertEquals(200, $response->getStatusCode());
    }
}
